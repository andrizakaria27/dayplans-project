package com.muslimindonesia.dayplans.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.muslimindonesia.dayplans.R;
import com.muslimindonesia.dayplans.model.ImageChild;

import java.util.ArrayList;

public class ImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<ImageChild> childData;
    private Context context;

    ImageAdapter(Context ctx, ArrayList<ImageChild> childData) {
        this.childData = childData;
        this.context = ctx;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView tvImage;

        ViewHolder(View itemView) {
            super(itemView);
            tvImage = itemView.findViewById(R.id.tv_image);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_image, parent, false);
        return new ViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final ViewHolder vh = (ViewHolder) holder;
        ImageChild imageChild = childData.get(position);
        int id = context.getResources().getIdentifier(context.getPackageName() + ":drawable/" + imageChild.getImages(), null, null);
        vh.tvImage.setImageResource(id);

//        Glide.with(context)
//                .load(imageChild.getImages())
//                .placeholder(id)
//                .into(vh.tvImage);
    }

    @Override
    public int getItemCount() {
        return childData.size();
    }
}

