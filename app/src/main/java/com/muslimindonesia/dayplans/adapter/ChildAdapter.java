package com.muslimindonesia.dayplans.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.muslimindonesia.dayplans.R;
import com.muslimindonesia.dayplans.model.TextListChild;

import java.util.ArrayList;

public class ChildAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<TextListChild> textListChildData;
    private Context context;

    ChildAdapter(Context ctx, ArrayList<TextListChild> textListChildData) {
        this.textListChildData = textListChildData;
        this.context = ctx;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout relativeLayout;
        TextView tvChild, tvNumber, tvVisitor, tvDirectin, tvOpen;
        ImageView imgDirection, imgVisitor;

        ViewHolder(View itemView) {
            super(itemView);
            tvChild = itemView.findViewById(R.id.tv_child);
            tvNumber = itemView.findViewById(R.id.tv_number);
            tvVisitor = itemView.findViewById(R.id.tv_visitor);
            tvDirectin = itemView.findViewById(R.id.tv_direction);
            imgDirection = itemView.findViewById(R.id.direction);
            imgVisitor = itemView.findViewById(R.id.img_visitor);
            tvOpen = itemView.findViewById(R.id.tv_open);
            relativeLayout = itemView.findViewById(R.id.relative);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_child, parent, false);
        return new ViewHolder(itemLayoutView);
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final ViewHolder vh = (ViewHolder) holder;
        TextListChild c = textListChildData.get(position);
        vh.tvChild.setText(c.getChild_name());
        vh.tvNumber.setText(c.getChild_number());

        String visitor = c.getChild_visitor();
        if (visitor.equals("")){
            vh.imgVisitor.setVisibility(View.GONE);
            vh.tvVisitor.setVisibility(View.GONE);
        }else {
            vh.tvVisitor.setText(c.getChild_visitor());
        }

        String direction = c.getChild_direction();
        if (direction.equals("")){
            vh.imgDirection.setVisibility(View.GONE);
            vh.tvDirectin.setVisibility(View.GONE);
        } else {
            vh.tvDirectin.setText(c.getChild_direction());
        }

        String open = c.getChild_open();
        if(open.equals("")){
            vh.tvOpen.setVisibility(View.GONE);
        }else {
            vh.tvOpen.setText(c.getChild_open());
        }

        if (position % 2 == 1) {
            vh.relativeLayout.setBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            vh.relativeLayout.setBackgroundColor(context.getResources().getColor(R.color.textbg));
        }
    }

    @Override
    public int getItemCount() {
        return textListChildData.size();
    }
}
