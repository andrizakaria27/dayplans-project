package com.muslimindonesia.dayplans.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.muslimindonesia.dayplans.R;
import com.muslimindonesia.dayplans.model.ImageChild;
import com.muslimindonesia.dayplans.model.ParentChild;
import com.muslimindonesia.dayplans.model.TextListChild;
import com.muslimindonesia.dayplans.util.NestedRecyclerLinearLayoutManager;

import java.util.ArrayList;

public class ParentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<ParentChild> parentChildData;
    private Context ctx;

    public ParentAdapter(Context ctx, ArrayList<ParentChild> parentChildData) {
        this.ctx = ctx;
        this.parentChildData = parentChildData;
    }

    @Override
    public int getItemViewType(int position) {
        return parentChildData.get(position).getViewType();
    }

    private ParentChild getItem(int position) {
        return parentChildData.get(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView textView1, textView2, textView3;
        ImageView expand;
        RecyclerView rv_child, rv_image;
        boolean clicked;

        ViewHolder(View itemView) {
            super(itemView);
            rv_child = itemView.findViewById(R.id.rv_child);
            rv_image = itemView.findViewById(R.id.rv_image);
            expand = itemView.findViewById(R.id.expand);
            textView1 = itemView.findViewById(R.id.textView1);
            textView2 = itemView.findViewById(R.id.textView2);
            textView3 = itemView.findViewById(R.id.textView3);
            clicked = false;
        }
    }

    public static class HeaderHolder extends RecyclerView.ViewHolder {
        HeaderHolder(View view) {
            super(view);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case 1: {
                View v = inflater.inflate(R.layout.recycler_item_parent, parent, false);
                viewHolder = new ViewHolder(v);
                break;
            }
            case 2: {
                View v = inflater.inflate(R.layout.recycler_item_header, parent, false);
                viewHolder = new HeaderHolder(v);
                break;
            }

        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItem(position).getViewType() == 1) {
            final ViewHolder vh = (ViewHolder) holder;
            vh.textView1.setText(getItem(position).getText1());
            vh.textView2.setText(getItem(position).getText2());
            vh.textView3.setText(getItem(position).getText3());

            initChildLayoutManager(vh.rv_child, getItem(position).getTextListChild());
            initImageLayoutManager(vh.rv_image, getItem(position).getImageChild());

            vh.rv_child.setVisibility(View.GONE);
            vh.clicked = true;

            vh.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (vh.clicked) {
                        vh.rv_child.setVisibility(View.VISIBLE);
                        vh.expand.setImageResource(R.drawable.ic_expand_less);
                        vh.clicked = false;
                    } else {
                        vh.rv_child.setVisibility(View.GONE);
                        vh.expand.setImageResource(R.drawable.ic_expand_more);
                        vh.clicked = true;
                    }
                }
            });
        }
    }

    private void initImageLayoutManager(RecyclerView rv_image, ArrayList<ImageChild> imageChild) {
        rv_image.setLayoutManager(new NestedRecyclerLinearLayoutManager(ctx, LinearLayoutManager.HORIZONTAL, false));
        ImageAdapter imageAdapter = new ImageAdapter(ctx, imageChild);
        rv_image.setAdapter(imageAdapter);
    }

    private void initChildLayoutManager(RecyclerView rv_child, ArrayList<TextListChild> textListChildData) {
        rv_child.setLayoutManager(new NestedRecyclerLinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false));
        ChildAdapter childAdapter = new ChildAdapter(ctx, textListChildData);
        rv_child.setAdapter(childAdapter);
    }

    @Override
    public int getItemCount() {
        return parentChildData.size();
    }
}
