package com.muslimindonesia.dayplans.data;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;

import com.muslimindonesia.dayplans.R;
import com.muslimindonesia.dayplans.model.ImageChild;
import com.muslimindonesia.dayplans.model.ParentChild;
import com.muslimindonesia.dayplans.model.TextListChild;

import java.util.ArrayList;

public class DataGenerator {

    @SuppressLint("Recycle")
    public static ArrayList<ParentChild> createData(Context ctx) {
        ArrayList<ParentChild> parentChildObj = new ArrayList<>();

        //menambahakan header
        ParentChild headerModel = new ParentChild();
        headerModel.setViewType(2);
        parentChildObj.add(0,headerModel);

        //Data 1

        ArrayList<ImageChild> listImage1 = new ArrayList<>();
        TypedArray imaget_1 = ctx.getResources().obtainTypedArray(R.array.number1);
        String[] image_1 = ctx.getResources().getStringArray(R.array.imagetxt1);
        for (int i = 0; i < imaget_1.length(); i++) {
            ImageChild imageChild1 = new ImageChild();
            imageChild1.setImages(image_1[i]);
            listImage1.add(imageChild1);
        }

        ArrayList<TextListChild> list1 = new ArrayList<>();
        ParentChild parentChild1 = new ParentChild();

        TypedArray number_1 = ctx.getResources().obtainTypedArray(R.array.number1);
        String[] text_1 = ctx.getResources().getStringArray(R.array.text1);
        String[] visitor_1 = ctx.getResources().getStringArray(R.array.visitor1);
        String[] directon_1 = ctx.getResources().getStringArray(R.array.direction1);
        String[] open_1 = ctx.getResources().getStringArray(R.array.open1);
        for (int i = 0; i < number_1.length(); i++) {
            TextListChild textListChild1 = new TextListChild();
            textListChild1.setChild_name(text_1[i]);
            textListChild1.setChild_visitor(visitor_1[i]);
            textListChild1.setChild_direction(directon_1[i]);
            textListChild1.setChild_open(open_1[i]);
            textListChild1.setChild_number(String.valueOf(i + 1));
            list1.add(textListChild1);

            parentChild1.setText2((i + 1) + " Sights");
        }

        parentChild1.setText1("Three Days in Paris");
        parentChild1.setText3("Some sights may not be open on Mon & Wed. Please check opening hours");
        parentChild1.setTextListChild(list1);
        parentChild1.setImageChild(listImage1);
        parentChild1.setViewType(1);
        parentChildObj.add(parentChild1);

        // ------- // ------- // ------- // ------- // ------- // ------- // -------

        //Data 2

        ArrayList<ImageChild> listImage2 = new ArrayList<>();
        TypedArray imaget_2 = ctx.getResources().obtainTypedArray(R.array.number2);
        String[] image_2 = ctx.getResources().getStringArray(R.array.imagetxt2);
        for (int i = 0; i < imaget_2.length(); i++) {
            ImageChild imageChild2 = new ImageChild();
            imageChild2.setImages(image_2[i]);
            listImage2.add(imageChild2);
        }

        ArrayList<TextListChild> list2 = new ArrayList<>();
        ParentChild parentChild2 = new ParentChild();

        TypedArray number_2 = ctx.getResources().obtainTypedArray(R.array.number2);
        String[] text_2 = ctx.getResources().getStringArray(R.array.text2);
        String[] visitor_2 = ctx.getResources().getStringArray(R.array.visitor2);
        String[] directon_2 = ctx.getResources().getStringArray(R.array.direction2);
        String[] open_2 = ctx.getResources().getStringArray(R.array.open2);
        for (int i = 0; i < number_2.length(); i++) {
            TextListChild textListChild2 = new TextListChild();
            textListChild2.setChild_name(text_2[i]);
            textListChild2.setChild_visitor(visitor_2[i]);
            textListChild2.setChild_direction(directon_2[i]);
            textListChild2.setChild_open(open_2[i]);
            textListChild2.setChild_number(String.valueOf(i + 1));
            list2.add(textListChild2);

            parentChild2.setText2((i + 1) + " Sights");
        }

        parentChild2.setText1("Main Sights");
        parentChild2.setText3("Some sights may not be open on Mon & Wed. Please check opening hours");
        parentChild2.setTextListChild(list2);
        parentChild2.setImageChild(listImage2);
        parentChild2.setViewType(1);
        parentChildObj.add(parentChild2);

        // ------- // ------- // ------- // ------- // ------- // ------- // -------

        //Data 3

        ArrayList<ImageChild> listImage3 = new ArrayList<>();
        TypedArray imaget_3 = ctx.getResources().obtainTypedArray(R.array.number3);
        String[] image_3 = ctx.getResources().getStringArray(R.array.imagetxt3);
        for (int i = 0; i < imaget_3.length(); i++) {
            ImageChild imageChild3 = new ImageChild();
            imageChild3.setImages(image_3[i]);
            listImage3.add(imageChild3);
        }

        ArrayList<TextListChild> list3 = new ArrayList<>();
        ParentChild parentChild3 = new ParentChild();

        TypedArray number_3 = ctx.getResources().obtainTypedArray(R.array.number3);
        String[] text_3 = ctx.getResources().getStringArray(R.array.text3);
        String[] visitor_3 = ctx.getResources().getStringArray(R.array.visitor3);
        String[] directon_3 = ctx.getResources().getStringArray(R.array.direction3);
        String[] open_3 = ctx.getResources().getStringArray(R.array.open3);
        for (int i = 0; i < number_3.length(); i++) {
            TextListChild textListChild3 = new TextListChild();
            textListChild3.setChild_name(text_3[i]);
            textListChild3.setChild_visitor(visitor_3[i]);
            textListChild3.setChild_direction(directon_3[i]);
            textListChild3.setChild_open(open_3[i]);
            textListChild3.setChild_number(String.valueOf(i + 1));
            list3.add(textListChild3);

            parentChild3.setText2((i + 1) + " Sights");
        }

        parentChild3.setText1("City Center Hightlights");
        parentChild3.setText3("Some sights may not be open on Mon & Wed. Please check opening hours");
        parentChild3.setTextListChild(list3);
        parentChild3.setImageChild(listImage3);
        parentChild3.setViewType(1);
        parentChildObj.add(parentChild3);

        return parentChildObj;

    }
}

