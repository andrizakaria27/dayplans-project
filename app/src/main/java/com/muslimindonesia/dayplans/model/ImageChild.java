package com.muslimindonesia.dayplans.model;

public class ImageChild {

    private String image;

    public ImageChild() {
    }

    public void setImages(String images) {
        this.image = images;
    }

    public String getImages() {
        return image;
    }

}
