package com.muslimindonesia.dayplans.model;

import java.io.Serializable;
import java.util.ArrayList;

public class ParentChild implements Serializable {

    private String text1, text2, text3;
    private Integer viewType;
    private ArrayList<TextListChild> textListChild;
    private ArrayList<ImageChild> imageChild;

    public ParentChild() {}

    public Integer getViewType() {
        return viewType;
    }

    public void setViewType(Integer viewType) {
        this.viewType = viewType;
    }

    public ArrayList<TextListChild> getTextListChild() {
        return textListChild;
    }

    public ArrayList<ImageChild> getImageChild() {
        return imageChild;
    }

    public void setTextListChild(ArrayList<TextListChild> textListChild) {
        this.textListChild = textListChild;
    }

    public void setImageChild(ArrayList<ImageChild> imageChild) {
        this.imageChild = imageChild;
    }


    public void setText1(String text1) {
        this.text1 = text1;
    }

    public void setText2(String text2) {
        this.text2 = text2;
    }

    public void setText3(String text3) {
        this.text3 = text3;
    }

    public String getText1() {
        return text1;
    }

    public String getText2() {
        return text2;
    }

    public String getText3() {
        return text3;
    }
}

