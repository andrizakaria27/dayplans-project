package com.muslimindonesia.dayplans.model;

public class TextListChild {

    private String child_name, child_number, child_visitor, child_direction, child_open;

    public TextListChild() {
    }

    public String getChild_name() {
        return child_name;
    }

    public void setChild_name(String child_name) {
        this.child_name = child_name;
    }

    public void setChild_number(String child_number) {
        this.child_number = child_number;
    }

    public String getChild_number() {
        return child_number;
    }

    public void setChild_visitor(String child_visitor) {
        this.child_visitor = child_visitor;
    }

    public String getChild_visitor() {
        return child_visitor;
    }

    public String getChild_direction() {
        return child_direction;
    }

    public void setChild_direction(String child_direction) {
        this.child_direction = child_direction;
    }

    public void setChild_open(String child_open) {
        this.child_open = child_open;
    }

    public String getChild_open() {
        return child_open;
    }
}
