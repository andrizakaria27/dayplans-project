package com.muslimindonesia.dayplans.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.muslimindonesia.dayplans.R;
import com.muslimindonesia.dayplans.adapter.ParentAdapter;
import com.muslimindonesia.dayplans.data.DataGenerator;
import com.muslimindonesia.dayplans.model.ParentChild;

import java.util.ArrayList;

public class ActivityDayPlans extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_plans);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setToolbar(toolbar);
        initComponent();

    }

    private void setToolbar(Toolbar toolbar) {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
        }
    }

    private void initComponent() {
        RecyclerView recyclerViewParent = findViewById(R.id.rv_parent);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewParent.setLayoutManager(manager);
        recyclerViewParent.setHasFixedSize(true);
        ArrayList<ParentChild> items = DataGenerator.createData(this);
        ParentAdapter parentAdapter = new ParentAdapter(this, items);
        recyclerViewParent.setAdapter(parentAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

}
